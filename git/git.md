# Git

## Das Respository aufräumen

<details>
<summary markdown="span">
Wie kann man eine Datei löschen, welche von Git getracked wird?
</summary>

`git rm file`
</details>

<details>
<summary markdown="span">
Wie kann man eine Datei löschen, welche von Git getracked wird und bereits zur
Staging Area hinzugefügt worden ist?
</summary>

`git rm -f file`
</details>

<details>
<summary markdown="span">
Wie kann man eine Ordner löschen, welcher von Git getracked wird?
</summary>

`git rm -r folder`
</details>

<details>
<summary markdown="span">
Wie kann man eine Datei in Git löschen, aber lokal eine Kopie behalten?
</summary>

`git rm --cached file`
</details>

<details>
<summary markdown="span">
Wie kann man eine Datei umbennen, welche von Git getracked wird?
</summary>

`git mv file_from file_to`
</details>

<details>
<summary markdown="span">
Wie kann man alle Dateien in einem Respository entfernen, welche von Git nicht
getracked werden?
</summary>

1. Schaue, welche Dateien betroffen sind: `git clean -n -d -x`
2. Lösche Dateien: `git clean -f -d -x`
</details>

<details>
<summary markdown="span">
Wie kann man Dateien interaktiv in einem Respository entfernen, welche von Git
nicht getracked werden?
</summary>

`git clean -x -i`
</details>

## Das Repository durchsuchen

<details>
<summary markdown="span">
Wie kann man alle "Person" Stellen im Repository finden? Es soll auch die
zugehörige Zeilennummer ausgegeben werden.
</summary>

`git grep -n Person`
</details>

<details>
<summary markdown="span">
Wie kann die Anzahl der "Person" Stellen in einer Datei ausgegeben werden?
</summary>

`git grep -c Person`
</details>

<details>
<summary markdown="span">
Wie kann man alle "Person" Stellen im Repository finden und den zugehörigen
Kontext ausgeben? 
</summary>

`git grep -p Person`
</details>

<details>
<summary markdown="span">
Wie kann man alle Defines in einem Repository ausgeben, welche entweder LINK
oder BUF_MAX enthalten?
</summary>

`git grep --break --heading -n -e '#define' --and \( -e LINK -e BUF_MAX \)`
</details>

## Die History durchsuchen

</details>

<details>
<summary markdown="span">
Wie kann man alle Stellen finden, an denen der Define "MAX" geändert oder
benutzt worden ist?
</summary>

`git log -S MAX --oneline`
</details>

<details>
<summary markdown="span">
Wie kann man alle Änderungen der Funktion "main" in der Datei module.cpp
einsehen?
</summary>

`git log -L :main:module.cpp`
</details>

<details>
<summary markdown="span">
Wie kann man den Log der Zeilen 101 bis 103 einer Datei einsehen?
</summary>

`git log -L101,+2:file_name`
</details>
